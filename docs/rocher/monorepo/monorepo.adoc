= Le mono-repo produit... c'est la vie ! : mono-repo vs multi-repo
:revealjs_customtheme: ../../framework/themes/css/reveal-rocher-leaves-dark.css
:toc-title: Sommaire

:includedir: _slides
:imagesdir: images
:workspace: ../student/workspace

ifdef::backend-html5[]
link:{docname}.htm[icon:video-camera[] Slides]  link:{docname}.pdf[icon:file-pdf-o[] PDF] link:{docname}.adoc[icon:file-text-o[] Source]
endif::backend-html5[]

ifdef::backend-revealjs[]
[%notitle]
== Sommaire
toc::[]
endif::backend-revealjs[]

ifdef::backend-revealjs[]
[.bottom]
link:{docname}.pdf[icon:file-pdf-o[] PDF]  link:{docname}.html[icon:globe[] HTML]  link:{docname}.adoc[icon:file-alt[] Source]
endif::backend-revealjs[]

[.impact]
== Définition

"Mono-repo : (Anglicisme informatique) (Programmation informatique) Mode de développement d'un programme sur un dépôt unique (par opposition à une séparation en plusieurs packages)."
-- Wiktionnaire

== Mono-repo VS Multi-repo

Sur les internets, la bataille fait rage 😖🥊

* link:https://medium.com/@mattklein123/monorepos-please-dont-e9a279be011b[Monorepos: Please don’t!]
* link:https://medium.com/@adamhjk/monorepo-please-do-3657e08a4b70[Monorepos: Please do!]
* link:https://medium.com/criteo-labs/why-you-dont-need-a-mono-repo-but-could-just-build-from-source-26bec7502af5[Why you don’t need a mono-repo but should just build from source]
* link:https://github.com/joelparkerhenderson/monorepo_vs_polyrepo[Monorepo vs. polyrepo]
* link:https://medium.com/outbrain-engineering/mono-repository-or-poly-repo-we-go-hybrid-314e1e17a7dd[Mono Repository or Poly Repo? We go Hybrid!]
* link:https://www.yegor256.com/2018/09/05/monolithic-repositories.html[Monolithic Repos Are Evil]

== Différentes organisations d'entreprise

[.maxed-image]
image::cicd-git.jpg[]

=== Un produit, plusieurs équipes

[.maxed-image]
include::_includes/1-comp-prod-multi-team.puml.adoc[]

=== Un produit, plusieurs équipes

[.maxed-image]
include::_includes/2-repo-comp-prod-multi-team.puml.adoc[]

=== Une équipe, plusieurs produits

[.maxed-image]
include::_includes/3-comp-team-multi-prod.puml.adoc[]

=== Un produit, une équipe

Scénario simplifié considéré comme représentant notre entreprise.

[.big-image]
include::_includes/4-comp-one-team-one-prod.puml.adoc[]

ifdef::backend-revealjs[]
[.questions]
=== !
endif::backend-revealjs[]

== Mono-repo entreprise

[.maxed-image]
image::cicd-dev.jpg[]

=== Mono-repo entreprise : avantages

[%step]
* pas de version de dépendances interne à maintenir
* pas de risque de dépendre de 2 versions d'une même dépendance interne/externe
* une seule merge request pour une mise à jour de dépendance interne
* build en continu simplifié
* intégration continue simplifiée
* recherche de code simplifiée
* analyse d'impact simplifié
* mise en commun de code simplifiée

=== Mono-repo entreprise : inconvénients

[%step]
* coordination importante
* outillage à déveloper en interne
* besoin élevé en resources (CPU, etc.)
* nécessite une couverture de test élevée
* le comportement des projets est impacté instantanément sur modification d'une dépendance
* L'accès est donné à l'ensemble du code source de l'entreprise

ifdef::backend-revealjs[]
[.questions]
=== !
endif::backend-revealjs[]

== Multi-repo modules (MuR) VS Mono-repo produit (MoRP)

[.maxed-image]
image::pilules-matrix.jpg[]

=== MuR : modification majeure de librairie interne

[.maxed-image]
include::_includes/mur-change-transverse.puml.adoc[]

=== MoRP : modification majeure de librairie interne

[.maxed-image]
include::_includes/morp-change-transverse.puml.adoc[]

=== MoRP : que des avantages sur le MuR

[%step]
* les avantages du mono-repo entreprise évoqué précédemment
* des modifications transverses grandement simplifiées (on l'a vu)
* des tests d'intégration / E2E pré-merge
* => une application logique du _Don't Repeat Yourself_ (DRY)

ifdef::backend-revealjs[]
[.questions]
=== !
endif::backend-revealjs[]

== Comment atteindre l'intégration continue ?

[.maxed-image]
image::cicd-idea.jpg[]

=== Pratique complémentaire 1/3 : gitflow

[.maxed-image]
image::gitflow.png[]

=== Pratique complémentaire 2/3 : trunk-based development

[.maxed-image]
image::tbd.png[]

=== Pratique complémentaire 3/3 : Feature-flipping

[.maxed-image]
image::feature-flipping.png[]

=== Les pratiques conditionnent la fréquence

[%step]
* multi-repo modules + _gitflow_ => *intégration occasionnelle*
* mono-repo produit + _gitflow_ => *intégration fréquente*
* multi-repo modules + outillage spécifique + _trunk-based development_ + _feature-flipping_ => *intégration fréquente à continue*
* mono-repo produit + _trunk-based development_ + _feature-flipping_ => *intégration continue*

ifdef::backend-revealjs[]
[.questions]
=== !
endif::backend-revealjs[]

== Mono-repo produit : exemples

Exemples de mono-repo produit sous Gitlab et AWS/EKS (Kubernetes managé) : link:https://github.com/Zenika/zenixample[Zenixample (2019)] et link:https://gitlab.com/webvia/webvia[Webvia (2020)]

ifdef::backend-revealjs[]
[.questions]
=== !
endif::backend-revealjs[]

== Comment faire la transition vers un mono-repo produit ?

[%step]
* mettre en place un mono-repo sans impact grâce aux _submodules_ git
* mettre en place du déploiement Kubernetes / GCP et des tests (simples) bout-en-bout
* migrer progressivement les projets en remplaçant les liens vers les _submodules_
* possibilité de garder les historiques git avec `git merge --allow-unrelated-histories`

ifdef::backend-revealjs[]
[.bubbles]
== !
endif::backend-revealjs[]
